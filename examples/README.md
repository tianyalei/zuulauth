# 一、前言
本示例是基于 **1bb9db3cbf** 版本编写的demo示例，方便小伙伴进行测试。
~~~xml
<dependency>
     <groupId>com.github.tianyaleixiaowu</groupId>
     <artifactId>zuulauth</artifactId>
     <version>1bb9db3cbf</version>
</dependency>
~~~
# 二、项目结构
- order-center：订单服务 用于测试权限 ，       **端口：8081**
- zuul-server： zuul网关，用于统一的鉴权服务，  **端口：8082**
- auth-server： 授权服务，用于管理修改权限，  **端口：8083**

# 三、启动测试
 先启动 order-center 再启动zuul-sever，最后启动auth-server 防止授权信息无法同步到 zuul-server 导致鉴权失败，发送请求，保存一些权限信息，用于测试。 
1. 新建一个角色和权限的关系，使得roleId=2001 包含 order-center中 **/getOrderById** 接口的访问权限

post请求：
~~~url
localhost:8082/permission/saveRolePermission
~~~
请求体：
~~~json
{
  "roleId": 2001,
  "permissionList": [
    {
      "menuId": 3001,
      "uri": "/getOrderById",
      "url": "order-center/getOrderById"
    }
  ]
}
~~~
2. 给userId用户赋予 roleId=2001 的角色

post请求：
~~~url
localhost:8082/permission/saveUserRoles
~~~
请求体：
~~~json
{
	"userId": 1,
	"roleList": [
		{
			"roleId": 2001,
			"roleName": "超级管理员",
			"roleCode": "admin"
		}
	]
}

~~~
3. 由于本demo使用token进行验证，首选进行登录获取 token
post请求：
~~~url
localhost:8083/app/login
~~~
请求体：
~~~json
{
    "mobile": "18888888888",
    "password": "123456"
}
~~~

返回值：
~~~json

{
    "code": 0,
    "data": {
        "expire": 604800,
        "token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxIiwiaWF0IjoxNjQ5NTk2NTQ1LCJleHAiOjE2NTAyMDEzNDV9.qnRbl_M7_6jU8V_gOGJ6xvMMSA_1MluBl-rkdBZL0rqEVBcsITn_CFhAqm21cHeDgI6OiFNIE_2mrcNxxwXp0A"
    },
    "msg": "执行成功"
}

~~~
4. 测试通过 zuul-server 鉴权成功后，然后转发到 order-center 进行业务的处理：
get请求:
~~~url
localhost:8082/order-center/getOrderById
~~~
请求头：
~~~yml
token：eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzUxMiJ9.eyJzdWIiOiIxIiwiaWF0IjoxNjQ5NTk2NTQ1LCJleHAiOjE2NTAyMDEzNDV9.qnRbl_M7_6jU8V_gOGJ6xvMMSA_1MluBl-rkdBZL0rqEVBcsITn_CFhAqm21cHeDgI6OiFNIE_2mrcNxxwXp0A
~~~
params:
~~~yml
orderId:11111
~~~
返回结果：
![img.png](img.png)

由此可见，我们访问的是zuul-sever（8082），鉴权成功后 zuul把请求转发到了 order-center（8081）服务上了，返回了正常的业务结果。