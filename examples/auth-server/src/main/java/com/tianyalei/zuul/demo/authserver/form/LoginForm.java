

package com.tianyalei.zuul.demo.authserver.form;

import lombok.Data;

import javax.validation.constraints.NotBlank;

/**
 * 登录表单
 *
 * @author ratel
 */
@Data

public class LoginForm {

    @NotBlank(message="手机号不能为空")
    private String mobile;

    @NotBlank(message="密码不能为空")
    private String password;

}
