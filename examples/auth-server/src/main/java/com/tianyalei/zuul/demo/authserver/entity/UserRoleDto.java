package com.tianyalei.zuul.demo.authserver.entity;

import lombok.Data;

import java.io.Serializable;
import java.util.List;

@Data
public class UserRoleDto implements Serializable {

    private long userId;
    private List<Role> roleList;
}
