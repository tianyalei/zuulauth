package com.tianyalei.zuul.demo.authserver;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 *基于zuul的demo
 * demo工程-授权服务，用于管理修改权限
 * @author ratel 2022-04-10
 */
@SpringBootApplication
public class AuthServerApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuthServerApplication.class, args);
        System.out.println("启动成功！");
    }

}
