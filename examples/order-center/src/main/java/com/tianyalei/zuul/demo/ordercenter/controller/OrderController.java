package com.tianyalei.zuul.demo.ordercenter.controller;


import com.tianyalei.zuul.zuulauth.annotation.RequireCodes;
import com.tianyalei.zuul.zuulauth.annotation.RequireRoles;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 *
 * @author ratel 2022-04-10
 */
@RestController
public class OrderController {


    @RequestMapping("/getOrderById")
    @RequireRoles("order-admin")
    public String getOrderById(String orderId){
        return "这是orderId："+orderId+"，的详细订单呀！";
    }

    @RequestMapping("/getOrderById2")
    @RequireRoles("order-admin")
    public String getOrderById2(String orderId){
        return orderId+"22222";
    }

    @RequestMapping("/orderList")
    @RequireCodes("/myOrderList")
    public String list(){
        return "this my order list!";
    }


    @RequestMapping("/orderList2")
    @RequireCodes("/myOrderList")
    public String list2(){
        return "this my order list 2 !";
    }

}
